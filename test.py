import pandas as pd
import sys
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split

names = ['Age', 'Which-year are you studying in?', 'CGPA/ percentage', 'Have you worked core Java', 'Have you worked on MySQL or Oracle database',
         'Have you studied OOP Concepts', 'Rate your written communication skills [1-10]', 'Rate your verbal communication skills [1-10]', 'Label']

path = str(sys.argv[1])

data = pd.read_csv(path, usecols=names)
data = data.dropna(axis=1, how='all')
X, y = data.iloc[:, :-1], data.iloc[:, -1]

validation_size = 0.15
seed = 7
X_train, X_validation, Y_train, Y_validation = train_test_split(
    X, y, test_size=validation_size, random_state=seed)

RF = RandomForestClassifier(n_estimators=100, max_depth=2, random_state=0)
RF.fit(X_train, Y_train)

score = RF.score(X_validation, Y_validation)
print(round(score, 4))
